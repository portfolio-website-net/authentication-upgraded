﻿using System;
using System.Linq;
using System.Collections.Generic;
using AuthenticationExample.Models;
using Microsoft.EntityFrameworkCore;
using System.Security.Cryptography;
using AuthenticationExample.Helpers;
using AuthenticationExample.Services.Interfaces;
using Microsoft.Extensions.Options;

namespace AuthenticationExample.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly Context _context;
        private readonly Settings _settings;
        private readonly IAuthenticationLogService _authenticationLogService;

        public AuthenticationService(Context context, 
            IOptions<Settings> settings,
            IAuthenticationLogService authenticationLogService)
        {
            _context = context;
            _settings = settings.Value;
            _authenticationLogService = authenticationLogService;
        }

        public void PopulateUsersIfEmptyDatabase()
        {
            if (!_context.Users.Any())
            {
                var hashResultSha512 = GetHashedPasswordResult("$ecur3P@ssw0rd!");

                _context.Users.Add(new User {
                    Username = "admin",
                    PasswordHash = hashResultSha512.Digest,
                    PasswordSalt = hashResultSha512.Salt,
                    FirstName = "Test",
                    LastName = "Admin",
                    Email = "admin@domain.com",
                    Phone = "123-456-7890",
                    Claims = "Admin,Read",
                    CreatedById = 0,
                    CreatedDate = DateTime.Now
                });    

                _context.Users.Add(new User {
                    Username = "user",
                    PasswordHash = hashResultSha512.Digest,
                    PasswordSalt = hashResultSha512.Salt,
                    FirstName = "Test",
                    LastName = "User",
                    Email = "user@domain.com",
                    Phone = "123-456-7891",
                    Claims = "Read",
                    CreatedById = 0,
                    CreatedDate = DateTime.Now
                });            

                _context.SaveChanges();
            }
        }

        public HashWithSaltResult GetHashedPasswordResult(string password)
        {
            var pwHasher = new PasswordWithSaltHasher();
            var hashResultSha512 = pwHasher.HashWithSalt(password, 64, SHA512.Create());
            return hashResultSha512;
        }

        public bool Authenticate(string username, string password, bool isProfile = false)
        {
            var success = false;
            var user = _context.Users.FirstOrDefault(x => x.Username == username);
            if (user != null)
            {
                if (!string.IsNullOrEmpty(password))
                {
                    var pwHasher = new PasswordWithSaltHasher();
                    var hashResultSha512 = pwHasher.HashWithSalt(password, user.PasswordSalt, SHA512.Create());
                    if (hashResultSha512.Digest == user.PasswordHash)
                    {
                        if (!isProfile)
                        {
                            _authenticationLogService.LogMessage(user, EventNameType.SignIn);
                        }
                        
                        success = true;
                    }
                    else if (!isProfile)
                    {
                        _authenticationLogService.LogMessage(user, EventNameType.FailedSignIn);
                    }
                }
                else
                {
                    _authenticationLogService.LogMessage(user, EventNameType.FailedSignIn);
                }
            }
            else
            {
                _authenticationLogService.LogMessage(null, EventNameType.FailedSignIn, $"Username '{username}' does not exist");
            }

            return success;
        }        

        public string CreateSession(string username)
        {
            var result = string.Empty;
            var user = _context.Users.FirstOrDefault(x => x.Username == username);
            if (user != null)
            {
                var currentDate = DateTime.Now;
                var sessionGuid = System.Guid.NewGuid();
                _context.UserSessions.Add(new UserSession {
                    UserId = user.UserId,
                    SessionGuid = sessionGuid.ToString(),
                    ExpirationDate = currentDate.AddMinutes(_settings.SessionTime),
                    CreatedById = user.UserId.ToString(),
                    CreatedDate = DateTime.Now
                });                

                _context.SaveChanges();

                result = sessionGuid.ToString();
            }

            return result;
        }

        public bool IsValidSession(string sessionGuid)
        {
            var userSession = GetSession(sessionGuid);

            return userSession != null;
        }

        public bool EndSession(string sessionGuid)
        {
            var success = false;
            var userSession = GetSession(sessionGuid);
            if (userSession != null)
            {
                var currentDate = DateTime.Now;
                userSession.ExpirationDate = currentDate;

                var outdatedSessionDate = DateTime.Now.AddMinutes(-90);
                var outdatedUserSessions = _context.UserSessions.Where(x => x.ExpirationDate <= outdatedSessionDate);

                if (outdatedUserSessions.Any())
                {
                    _context.UserSessions.RemoveRange(outdatedUserSessions);
                }

                var outdatedAuthenticationLogDate = DateTime.Now.AddDays(-30);
                var outdatedAuthenticationLogs = _context.AuthenticationLogs.Where(x => x.CreatedDate <= outdatedAuthenticationLogDate);

                if (outdatedAuthenticationLogs.Any())
                {
                    _context.AuthenticationLogs.RemoveRange(outdatedAuthenticationLogs);
                }

                _authenticationLogService.LogMessage(new User { UserId = userSession.UserId.Value }, EventNameType.SignOut);

                _context.SaveChanges();

                success = true;
            }            

            return success;
        }

        public bool RenewSession(string sessionGuid)
        {
            var success = false;
            var userSession = GetSession(sessionGuid);
            if (userSession != null)
            {
                var currentDate = DateTime.Now;
                userSession.ExpirationDate = currentDate.AddMinutes(_settings.SessionTime);
                userSession.UpdatedById = userSession.UserId.ToString();
                userSession.UpdatedDate = DateTime.Now;

                _context.SaveChanges();

                success = true;
            }            

            return success;
        }        

        public UserSession GetSession(string sessionGuid)
        {
            var currentDate = DateTime.Now;
            var userSession = _context.UserSessions
                .FirstOrDefault(x => x.SessionGuid == sessionGuid && x.ExpirationDate > currentDate);
            
            return userSession;
        }

        public User GetCurrentUser(string sessionGuid)
        {
            User user = null;
            if (!string.IsNullOrEmpty(sessionGuid))
            {
                var userSession = GetSession(sessionGuid);
                if (userSession != null)
                {
                    user = _context.Users.FirstOrDefault(x => x.UserId == userSession.UserId);
                }
            }
            
            return user;
        }
    }
}
