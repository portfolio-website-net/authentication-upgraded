﻿using AuthenticationExample.Models;
using AuthenticationExample.Helpers;

namespace AuthenticationExample.Services.Interfaces
{
    public interface IAuthenticationService
    {
        void PopulateUsersIfEmptyDatabase();      

        HashWithSaltResult GetHashedPasswordResult(string password);

        bool Authenticate(string username, string password, bool isProfile = false);          

        string CreateSession(string username);

        bool IsValidSession(string sessionGuid);

        bool EndSession(string sessionGuid);

        bool RenewSession(string sessionGuid);

        UserSession GetSession(string sessionGuid);

        User GetCurrentUser(string sessionGuid);
    }
}
