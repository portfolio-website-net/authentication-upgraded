﻿using AuthenticationExample.Models;
using AuthenticationExample.Helpers;

namespace AuthenticationExample.Services.Interfaces
{
    public interface IUserProfileService
    {
        UserProfileUpdateResult UpdateUserProfile(ProfileFields profileFields);
    }
}
