﻿using AuthenticationExample.Models;
using AuthenticationExample.Helpers;

namespace AuthenticationExample.Services.Interfaces
{
    public interface IAuthenticationLogService
    {
        void LogMessage(User user, EventNameType eventNameType, string message = null);
    }
}
