﻿using AuthenticationExample.Models;
using AuthenticationExample.Helpers;
using System.Collections.Generic;
using AuthenticationExample.Helpers.IAMObjects;

namespace AuthenticationExample.Services.Interfaces
{
    public interface IIAMService
    {
        bool IsUserReadyForMigration(string username);

        bool IsUserMigrated(string username);
        
        bool MigrateUser(string username, string password);

        bool HasUserIAMSession();

        UserProfile GetCachedUserProfileById(string userId);

        List<Group> GetCachedUserGroupsById(string userId);

        List<Group> GetCachedAllGroups();

        string GetCurrentUserId();

        User GetCachedUser();

        bool UpdateUserGroups(string userId, List<string> groups);
        
        UserProfileUpdateResult UpdateUserProfile(ProfileFields profileFields);

        bool Authenticate(string username, string password);

        string CreateSession();

        bool IsValidSession(string sessionGuid);

        bool EndSession(string sessionGuid);

        bool RenewSession(string sessionGuid);

        UserSession GetSession(string sessionGuid);

        User GetCurrentUser(string sessionGuid);
    }
}
