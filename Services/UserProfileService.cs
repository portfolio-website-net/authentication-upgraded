﻿using System;
using System.Linq;
using System.Collections.Generic;
using AuthenticationExample.Models;
using Microsoft.EntityFrameworkCore;
using System.Security.Cryptography;
using AuthenticationExample.Helpers;
using AuthenticationExample.Services.Interfaces;

namespace AuthenticationExample.Services
{
    public class UserProfileService : IUserProfileService
    {
        private readonly Context _context;
        private readonly IAuthenticationService _authenticationService;
        private readonly IAuthenticationLogService _authenticationLogService;

        public UserProfileService(
            Context context, 
            IAuthenticationService authenticationService,
            IAuthenticationLogService authenticationLogService)
        {
            _context = context;
            _authenticationService = authenticationService;
            _authenticationLogService = authenticationLogService;
        }
        
        public UserProfileUpdateResult UpdateUserProfile(ProfileFields profileFields)
        {
            var result = new UserProfileUpdateResult
            {
                Reason = UserProfileUpdateResultReason.Error
            };
            var user = _context.Users.FirstOrDefault(x => x.UserId == profileFields.UserId);
            if (user != null)
            {
                if (!string.IsNullOrEmpty(profileFields.CurrentPassword)
                    && _authenticationService.Authenticate(user.Username, profileFields.CurrentPassword, isProfile: true))
                {
                    if (!string.IsNullOrEmpty(profileFields.Username))
                    {
                        if (!_context.Users.Any(x => x.UserId != profileFields.UserId && x.Username == profileFields.Username))
                        {
                            if (user.Username != profileFields.Username)
                            {
                                _authenticationLogService.LogMessage(user, EventNameType.UsernameUpdate);
                                user.Username = profileFields.Username;
                            }
                        }
                        else
                        {
                            _authenticationLogService.LogMessage(user, EventNameType.UsernameMustBeUnique);
                            result.Reason = UserProfileUpdateResultReason.UsernameMustBeUnique;
                        }                        
                    }

                    if (!string.IsNullOrEmpty(profileFields.Password)
                        && profileFields.Password == profileFields.PasswordConfirm)
                    {
                        var hashResultSha512 = _authenticationService.GetHashedPasswordResult(profileFields.Password);
                        
                        _authenticationLogService.LogMessage(user, EventNameType.PasswordUpdate);
                        user.PasswordHash = hashResultSha512.Digest;
                        user.PasswordSalt = hashResultSha512.Salt;
                    }
                    else if (!string.IsNullOrEmpty(profileFields.Password))
                    {
                        _authenticationLogService.LogMessage(user, EventNameType.PasswordsDoNotMatch);
                        result.Reason = UserProfileUpdateResultReason.PasswordsDoNotMatch;
                    }

                    if (!string.IsNullOrEmpty(profileFields.FirstName))
                    {
                        user.FirstName = profileFields.FirstName;
                    }

                    if (!string.IsNullOrEmpty(profileFields.LastName))
                    {
                        user.LastName = profileFields.LastName;
                    }

                    if (!string.IsNullOrEmpty(profileFields.Email))
                    {
                        user.Email = profileFields.Email;
                    }

                    if (!string.IsNullOrEmpty(profileFields.Phone))
                    {
                        user.Phone = profileFields.Phone;
                    }

                    if (profileFields.Claims != null
                        && profileFields.Claims.Any())
                    {
                        var newClaims = string.Join(",", profileFields.Claims);
                        if (user.Claims != newClaims)
                        {
                            _authenticationLogService.LogMessage(user, EventNameType.PermissionUpdate, newClaims);
                            user.Claims = string.Join(",", profileFields.Claims);
                        }
                    }

                    if (result.Reason == UserProfileUpdateResultReason.Error)
                    {
                        user.UpdatedById = user.UserId;
                        user.UpdatedDate = DateTime.Now;

                        _authenticationLogService.LogMessage(user, EventNameType.ProfileUpdate);
                        _context.SaveChanges();

                        result.Reason = UserProfileUpdateResultReason.Success;
                    }
                }        
                else
                {
                    _authenticationLogService.LogMessage(user, EventNameType.InvalidProfilePassword);
                    result.Reason = UserProfileUpdateResultReason.InvalidCurrentPassword;
                }       
            }
            else
            {
                _authenticationLogService.LogMessage(user, EventNameType.ProfileUpdateError);
            } 

            return result;
        }
    }
}
