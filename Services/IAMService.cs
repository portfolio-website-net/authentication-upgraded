﻿using System;
using System.Linq;
using System.Collections.Generic;
using AuthenticationExample.Models;
using Microsoft.EntityFrameworkCore;
using System.Security.Cryptography;
using AuthenticationExample.Helpers;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Serialization;
using System.Net;
using AuthenticationExample.Helpers.IAMObjects;
using Microsoft.AspNetCore.Authentication;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using Interfaces = AuthenticationExample.Services.Interfaces;

namespace AuthenticationExample.Services
{
    public class IAMService : Interfaces.IIAMService
    {
        private readonly Context _context;
        private readonly Interfaces.IAuthenticationService _authenticationService;
        private readonly Interfaces.IAuthenticationLogService _authenticationLogService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IMemoryCache _memoryCache;
        private readonly Settings _settings;

        public IAMService(
            Context context, 
            Interfaces.IAuthenticationService authenticationService,
            Interfaces.IAuthenticationLogService authenticationLogService,
            IHttpContextAccessor httpContextAccessor,
            IMemoryCache memoryCache,
            IOptions<Settings> settings)
        {
            _context = context;
            _authenticationService = authenticationService;
            _authenticationLogService = authenticationLogService;
            _httpContextAccessor = httpContextAccessor;
            _memoryCache = memoryCache;
            _settings = settings.Value;
        }

        public bool IsUserReadyForMigration(string username)
        {
            var result = false;
            var user = _context.Users.FirstOrDefault(x => x.Username == username && x.MigratedUserId == null);
            if (user != null)
            {
                var migrationFeatureFlag = _context.FeatureFlagTypes.FirstOrDefault(x => x.Code == "EnableUserIAMMigration");
                if (migrationFeatureFlag != null)
                {
                    result = _context.FeatureFlags.Any(x => x.FeatureFlagTypeId == migrationFeatureFlag.FeatureFlagTypeId
                        && x.UserId == user.UserId);
                }
            }

            return result;
        }

        public bool IsUserMigrated(string username)
        {
            var result = false;
            var user = _context.Users.FirstOrDefault(x => x.Username == username && x.MigratedUserId != null);
            if (user != null)
            {
                result = true;
            }

            return result;
        }

        public bool MigrateUser(string username, string password)
        {
            var result = false;
            if (IsUserReadyForMigration(username)
                && _authenticationService.Authenticate(username, password))
            {
                var user = _context.Users.FirstOrDefault(x => x.Username == username);
                if (user != null)
                {
                    // dotnet add package RestSharp --version 106.6.10
                    var client = new RestClient(_settings.IAMDomain);
                    var request = new RestRequest("/api/v1/users?activate=true", Method.POST);
                    Console.WriteLine("RestRequest: /api/v1/users?activate=true");
                    request.AddHeader("Authorization", $"SSWS {_settings.ApiKey}");

                    var groupIds = new List<string>();
                    var allGroups = GetCachedAllGroups();
                    if (user.ClaimsList.Any(x => x.Trim() == "Admin"))
                    {
                        groupIds.Add(allGroups.First(x => x.Profile.Name == "Admin").Id);
                    }                 

                    if (user.ClaimsList.Any(x => x.Trim() == "Read"))
                    {
                        groupIds.Add(allGroups.First(x => x.Profile.Name == "Read").Id);
                    }   

                    var newUser = new CreateUser
                    {
                        Profile = new Profile
                        {
                            FirstName = user.FirstName,
                            LastName = user.LastName,
                            Email = user.Email,
                            Username = user.Username,
                            PrimaryPhone = user.Phone
                        },
                        Credentials = new Credentials
                        {
                            Password = new Password
                            {
                                Value = password
                            }
                        },
                        GroupIds = groupIds
                    };

                    request.AddParameter("application/json", JsonConvert.SerializeObject(newUser), ParameterType.RequestBody);
                    var response = client.Execute(request);

                    Console.WriteLine(response.Content);

                    if (response.IsSuccessful)
                    {
                        var userResult = JsonConvert.DeserializeObject<CreateUser>(response.Content);
                        if (!string.IsNullOrEmpty(userResult.Id))
                        {
                            user.MigratedUserId = userResult.Id;
                            user.UpdatedDate = DateTime.Now;
                            _context.SaveChanges();                            

                            result = true;

                            _authenticationLogService.LogMessage(user, EventNameType.SuccessfulMigration);

                            EnrollSMSFactor(user);
                        }
                    }

                    if (!result)
                    {
                        var migrationFeatureFlag = _context.FeatureFlagTypes.FirstOrDefault(x => x.Code == "EnableUserIAMMigration");
                        if (migrationFeatureFlag != null)
                        {
                            var userFeatureFlag = _context.FeatureFlags.FirstOrDefault(x => x.FeatureFlagTypeId == migrationFeatureFlag.FeatureFlagTypeId
                                && x.UserId == user.UserId);
                            _context.FeatureFlags.Remove(userFeatureFlag);

                            _authenticationLogService.LogMessage(user, EventNameType.FailedMigration);
                        }
                    }
                }
            }

            return result;
        }

        private bool EnrollSMSFactor(User user)
        {
            var result = false;
            var factor = new 
            {
                factorType = "sms",
                provider = IAMConfiguration.IAMProviderName,
                profile = new 
                {
                    phoneNumber = user.Phone
                }
            };                            

            var client = new RestClient(_settings.IAMDomain);
            var request = new RestRequest($"/api/v1/users/{user.MigratedUserId}/factors?activate=true&updatePhone=true", Method.POST);
            Console.WriteLine("RestRequest: /api/v1/users/{user.MigratedUserId}/factors?activate=true&updatePhone=true");
            request.AddHeader("Authorization", $"SSWS {_settings.ApiKey}");
            
            request.AddJsonBody(factor);
            var response = client.Execute(request);

            Console.WriteLine(response.Content);

            if (response.IsSuccessful)
            {
                _authenticationLogService.LogMessage(user, EventNameType.SMSFactorEnrollment);
                result = true;
            }
            else
            {
                _authenticationLogService.LogMessage(user, EventNameType.SMSFactorEnrollmentError);
            }

            return result;
        }

        private string GetSMSFactorId(User user)
        {
            var result = string.Empty;
            var client = new RestClient(_settings.IAMDomain);
            var request = new RestRequest($"/api/v1/users/{user.MigratedUserId}/factors", Method.GET);
            Console.WriteLine("RestRequest: /api/v1/users/{user.MigratedUserId}/factors");
            request.AddHeader("Authorization", $"SSWS {_settings.ApiKey}");            
            var response = client.Execute(request);

            Console.WriteLine(response.Content);

            if (response.IsSuccessful)
            {
                var factors = JsonConvert.DeserializeObject<List<Factor>>(response.Content);
                var smsFactor = factors.FirstOrDefault(x => x.FactorType == "sms");
                if (smsFactor != null)
                {
                    result = smsFactor.Id;
                }
            }

            return result;
        }

        private bool RemoveSMSFactor(User user, string factorId)
        {
            var result = false;
            var client = new RestClient(_settings.IAMDomain);
            var request = new RestRequest($"/api/v1/users/{user.MigratedUserId}/factors/{factorId}", Method.DELETE);
            Console.WriteLine("RestRequest: /api/v1/users/{user.MigratedUserId}/factors/{factorId}");
            request.AddHeader("Authorization", $"SSWS {_settings.ApiKey}");            
            var response = client.Execute(request);

            Console.WriteLine(response.Content);

            if (response.IsSuccessful)
            {
                result = true;
            }

            return result;
        }

        private bool UpdateSMSFactor(User user)
        {
            var result = false;

            var factorId = GetSMSFactorId(user);
            if (!string.IsNullOrEmpty(factorId))
            {
                result = RemoveSMSFactor(user, factorId);
                if (result)
                {
                    result = EnrollSMSFactor(user);
                }
            }
            else
            {
                result = EnrollSMSFactor(user);
            }
            
            return result;
        }

        public bool HasUserIAMSession()
        {
            return _httpContextAccessor.HttpContext.User.Claims.Any(x => x.Type == "sub");
        }

        private UserProfile GetUserProfile(string username)
        {
            var userProfile = new UserProfile();
            var client = new RestClient(_settings.IAMDomain);
            var request = new RestRequest($"/api/v1/users/{WebUtility.UrlEncode(username)}", Method.GET);
            Console.WriteLine("RestRequest: /api/v1/users/{WebUtility.UrlEncode(username)}");
            request.AddHeader("Authorization", $"SSWS {_settings.ApiKey}");            
            var response = client.Execute(request);

            Console.WriteLine(response.Content);

            if (response.IsSuccessful)
            {
                userProfile = JsonConvert.DeserializeObject<UserProfile>(response.Content);
            }

            return userProfile;
        }
        
        private UserProfile GetUserProfileById(string userId)
        {
            var userProfile = new UserProfile();
            var client = new RestClient(_settings.IAMDomain);
            var request = new RestRequest($"/api/v1/users/{userId}", Method.GET);
            Console.WriteLine("RestRequest: /api/v1/users/{userId}");
            request.AddHeader("Authorization", $"SSWS {_settings.ApiKey}");            
            var response = client.Execute(request);

            Console.WriteLine(response.Content);

            if (response.IsSuccessful)
            {
                userProfile = JsonConvert.DeserializeObject<UserProfile>(response.Content);
            }

            return userProfile;
        }

        public UserProfile GetCachedUserProfileById(string userId)
        {
            var cacheKey = userId + "_UserProfileById";
            UserProfile userProfile;
            if (_memoryCache.TryGetValue(cacheKey, out userProfile))
            {
                return userProfile;
            }
            else
            {
                userProfile = GetUserProfileById(userId);
                _memoryCache.Set(cacheKey, userProfile);
                return userProfile;
            }
        }

        private List<Group> GetUserGroupsById(string userId)
        {
            var groups = new List<Group>();
            var client = new RestClient(_settings.IAMDomain);
            var request = new RestRequest($"/api/v1/users/{userId}/groups", Method.GET);
            Console.WriteLine("RestRequest: /api/v1/users/{userId}/groups");
            request.AddHeader("Authorization", $"SSWS {_settings.ApiKey}");            
            var response = client.Execute(request);

            Console.WriteLine(response.Content);

            if (response.IsSuccessful)
            {
                groups = JsonConvert.DeserializeObject<List<Group>>(response.Content);
                groups = groups.Where(x => x.Profile != null && x.Profile.Name != "Everyone").ToList();
            }

            return groups;
        }

        public List<Group> GetCachedUserGroupsById(string userId)
        {
            var cacheKey = userId + "_UserGroupsById";
            List<Group> userGroups;
            if (_memoryCache.TryGetValue(cacheKey, out userGroups))
            {
                return userGroups;
            }
            else
            {
                userGroups = GetUserGroupsById(userId);
                _memoryCache.Set(cacheKey, userGroups);
                return userGroups;
            }
        }

        private List<Group> GetAllGroups()
        {
            var groups = new List<Group>();
            var client = new RestClient(_settings.IAMDomain);
            var request = new RestRequest($"/api/v1/groups", Method.GET);
            Console.WriteLine("RestRequest: /api/v1/groups");
            request.AddHeader("Authorization", $"SSWS {_settings.ApiKey}");            
            var response = client.Execute(request);

            Console.WriteLine(response.Content);

            if (response.IsSuccessful)
            {
                groups = JsonConvert.DeserializeObject<List<Group>>(response.Content);
                groups = groups.Where(x => x.Profile != null && x.Profile.Name != "Everyone").ToList();
            }

            return groups;
        }

        public List<Group> GetCachedAllGroups()
        {
            var cacheKey = "AllGroups";
            List<Group> allGroups;
            if (_memoryCache.TryGetValue(cacheKey, out allGroups))
            {
                return GetAllGroups();
            }
            else
            {
                allGroups = GetAllGroups();
                _memoryCache.Set(cacheKey, allGroups);
                return allGroups;
            }
        }

        public string GetCurrentUserId()
        {
            var claims = _httpContextAccessor.HttpContext.User.Claims;
            var sub = claims.FirstOrDefault(x => x.Type == "sub");
            if (sub != null)
            {
                return sub.Value;
            }
            else
            {
                return null;
            }
        }

        private User GetUser()
        {
            var user = new User();
            var claims = _httpContextAccessor.HttpContext.User.Claims;
            var sub = claims.FirstOrDefault(x => x.Type == "sub");
            if (sub != null)
            {
                var userId = sub.Value;
                var userProfile = GetCachedUserProfileById(userId);

                if (!string.IsNullOrEmpty(userProfile.Id))
                {
                    user.MigratedUserId = userId;
                    user.Username = userProfile.Profile.Username;
                    user.FirstName = userProfile.Profile.FirstName;
                    user.LastName = userProfile.Profile.LastName;
                    user.Email = userProfile.Profile.Email;
                    user.Phone = userProfile.Profile.PrimaryPhone;

                    var userGroups = GetCachedUserGroupsById(userId).Where(x => x.Profile != null).Select(x => x.Profile.Name);
                    user.Claims = string.Join(",", userGroups);                    
                }
            }

            return user;
        }

        public User GetCachedUser()
        {
            var userId = GetCurrentUserId();
            if (userId != null)
            {
                var cacheKey = userId + "_User";
                User user;
                if (_memoryCache.TryGetValue(cacheKey, out user))
                {
                    return user;
                }
                else
                {
                    user = GetUser();
                    _memoryCache.Set(cacheKey, user);
                    return user;
                }
            }
            else
            {
                return new User();
            }
        }

        public bool UpdateUserGroups(string userId, List<string> groups)
        {
            var result = true;
            var userGroups = GetCachedUserGroupsById(userId);
            var allGroups = GetCachedAllGroups();
            var existingGroupNames = groups;

            // Remove groups not included
            foreach (var groupToRemove in userGroups)
            {
                if (groupToRemove.Profile != null
                    && !existingGroupNames.Contains(groupToRemove.Profile.Name))
                {
                    var client = new RestClient(_settings.IAMDomain);
                    var request = new RestRequest($"/api/v1/groups/{groupToRemove.Id}/users/{userId}", Method.DELETE);
                    Console.WriteLine("RestRequest: /api/v1/groups/{groupToRemove.Id}/users/{userId}");
                    request.AddHeader("Authorization", $"SSWS {_settings.ApiKey}");
                    
                    var response = client.Execute(request);

                    Console.WriteLine(response.Content);

                    if (!response.IsSuccessful)
                    {
                        result = false;
                        break;
                    }
                }                
            }

            // Add groups included
            foreach (var groupName in existingGroupNames)
            {
                if (!userGroups.Where(x => x.Profile != null).Select(x => x.Profile.Name).Any(x => x == groupName)
                    && allGroups.Where(x => x.Profile != null).Select(x => x.Profile.Name).Any(x => x == groupName))
                {
                    var groupToAdd = allGroups.First(x => x.Profile.Name == groupName);
                    var client = new RestClient(_settings.IAMDomain);
                    var request = new RestRequest($"/api/v1/groups/{groupToAdd.Id}/users/{userId}", Method.PUT);
                    Console.WriteLine("RestRequest: /api/v1/groups/{groupToAdd.Id}/users/{userId}");
                    request.AddHeader("Authorization", $"SSWS {_settings.ApiKey}");
                    
                    var response = client.Execute(request);

                    Console.WriteLine(response.Content);

                    if (!response.IsSuccessful)
                    {
                        result = false;
                        break;
                    }
                }
            }

            return result;
        }
        
        public UserProfileUpdateResult UpdateUserProfile(ProfileFields profileFields)
        {
            var result = new UserProfileUpdateResult();
            var user = GetCachedUser();
            if (user != null
                && !string.IsNullOrEmpty(user.MigratedUserId))
            {
                if (!string.IsNullOrEmpty(profileFields.CurrentPassword)
                    && Authenticate(user.Username, profileFields.CurrentPassword))
                {
                    var userProfile = GetCachedUserProfileById(user.MigratedUserId);
                    if (!string.IsNullOrEmpty(userProfile.Id))
                    {
                        if (!string.IsNullOrEmpty(profileFields.Username))
                        {
                            if (user.Username != profileFields.Username)
                            {
                                _authenticationLogService.LogMessage(user, EventNameType.UsernameUpdate);
                                userProfile.Profile.Username = profileFields.Username;
                            }                            
                        }

                        if (!string.IsNullOrEmpty(profileFields.Password)
                            && profileFields.Password == profileFields.PasswordConfirm)
                        {
                            result = UpdatePassword(userProfile, profileFields, user);
                        }
                        else if (!string.IsNullOrEmpty(profileFields.Password))
                        {
                            _authenticationLogService.LogMessage(user, EventNameType.PasswordsDoNotMatch);
                            result.Reason = UserProfileUpdateResultReason.PasswordsDoNotMatch;
                        }

                        if (!string.IsNullOrEmpty(profileFields.FirstName))
                        {                            
                            userProfile.Profile.FirstName = profileFields.FirstName;
                        }

                        if (!string.IsNullOrEmpty(profileFields.LastName))
                        {
                            userProfile.Profile.LastName = profileFields.LastName;
                        }

                        if (!string.IsNullOrEmpty(profileFields.Email))
                        {
                            userProfile.Profile.Email = profileFields.Email;
                        }

                        if (result.Reason == UserProfileUpdateResultReason.None
                            && !string.IsNullOrEmpty(profileFields.Phone)
                            && userProfile.Profile.PrimaryPhone != profileFields.Phone)
                        {
                            userProfile.Profile.PrimaryPhone = profileFields.Phone;
                            user.Phone = userProfile.Profile.PrimaryPhone;
                            if (!UpdateSMSFactor(user))
                            {
                                result.Reason = UserProfileUpdateResultReason.SMSUpdateError;
                            }
                        }

                        if (result.Reason == UserProfileUpdateResultReason.None
                            && profileFields.Claims != null
                            && profileFields.Claims.Any())
                        {                            
                            if (profileFields.Claims.Intersect(user.ClaimsList).Count() != profileFields.Claims.Count()
                                || profileFields.Claims.Intersect(user.ClaimsList).Count() != user.ClaimsList.Count())
                            {
                                var newClaims = string.Join(",", profileFields.Claims);
                                
                                if (UpdateUserGroups(userProfile.Id, profileFields.Claims))
                                {
                                    _authenticationLogService.LogMessage(user, EventNameType.PermissionUpdate, newClaims);                                    
                                }
                                else
                                {
                                    _authenticationLogService.LogMessage(user, EventNameType.PermissionUpdateError, newClaims);
                                    result.Reason = UserProfileUpdateResultReason.PermissionUpdateError;
                                }

                                RemoveUserCacheObjects(user.Username);
                            }
                        }                            

                        if (result.Reason == UserProfileUpdateResultReason.None)
                        {                        
                            var client = new RestClient(_settings.IAMDomain);
                            var request = new RestRequest($"/api/v1/users/{userProfile.Id}", Method.POST);
                            Console.WriteLine("RestRequest: /api/v1/users/{userProfile.Id}");
                            request.AddHeader("Authorization", $"SSWS {_settings.ApiKey}");
                            
                            request.AddParameter("application/json", JsonConvert.SerializeObject(userProfile), ParameterType.RequestBody);
                            var response = client.Execute(request);

                            Console.WriteLine(response.Content);

                            if (response.IsSuccessful)
                            {
                                _authenticationLogService.LogMessage(user, EventNameType.ProfileUpdate);

                                if (user.Email != profileFields.Email
                                    && user.Username != profileFields.Email)
                                {
                                    // Update the username to match the entered value, 
                                    // since the IAM provider overrides the value with the email address.
                                    result = UpdateUsername(userProfile, profileFields, user);
                                }
                                else
                                {
                                    result.Reason = UserProfileUpdateResultReason.Success;                                
                                }
                            }
                            else
                            {
                                var errorMessage = string.Empty;
                                try
                                {
                                    var error = JsonConvert.DeserializeObject<Error>(response.Content);
                                    errorMessage = error.ErrorCauses.First().ErrorSummary;
                                }
                                catch
                                {                                    
                                }

                                if (errorMessage.Contains("An object with this field already exists in the current organization"))
                                {
                                    _authenticationLogService.LogMessage(user, EventNameType.UsernameMustBeUnique);
                                    result.Reason = UserProfileUpdateResultReason.UsernameMustBeUnique;
                                }
                                else
                                {
                                    result.ErrorMessage = errorMessage;
                                    result.Reason = UserProfileUpdateResultReason.Error;
                                }
                            }

                            RemoveUserCacheObjects(user.Username);
                        }
                        else
                        {
                            _authenticationLogService.LogMessage(user, EventNameType.ProfileUpdateError);
                        }  
                    }
                    else
                    {
                        _authenticationLogService.LogMessage(user, EventNameType.ProfileUpdateError);
                    }             
                }        
                else
                {
                    _authenticationLogService.LogMessage(user, EventNameType.InvalidProfilePassword);
                    result.Reason = UserProfileUpdateResultReason.InvalidCurrentPassword;
                }       
            }
            else
            {
                _authenticationLogService.LogMessage(user, EventNameType.ProfileUpdateError);
            }            

            return result;
        }

        private UserProfileUpdateResult UpdatePassword(UserProfile userProfile, ProfileFields profileFields, User user)
        {
            var result = new UserProfileUpdateResult();
            var userCredentials = new 
            {
                credentials = new
                {
                    password = new 
                    {
                        value = profileFields.Password
                    }
                }
            };                            

            var client = new RestClient(_settings.IAMDomain);
            var request = new RestRequest($"/api/v1/users/{userProfile.Id}", Method.POST);
            Console.WriteLine("RestRequest: /api/v1/users/{userProfile.Id}");
            request.AddHeader("Authorization", $"SSWS {_settings.ApiKey}");
            
            request.AddJsonBody(userCredentials);
            var response = client.Execute(request);

            Console.WriteLine(response.Content);

            if (response.IsSuccessful)
            {
                _authenticationLogService.LogMessage(user, EventNameType.PasswordUpdate);
                RemoveUserCacheObjects(user.Username);
            }
            else
            {
                var passwordErrorMessage = string.Empty;
                try
                {
                    var error = JsonConvert.DeserializeObject<Error>(response.Content);
                    passwordErrorMessage = error.ErrorCauses.First().ErrorSummary;
                }
                catch
                {                                    
                }                                

                _authenticationLogService.LogMessage(user, EventNameType.PasswordUpdateError, passwordErrorMessage);
                result.Reason = UserProfileUpdateResultReason.PasswordUpdateError;
                result.ErrorMessage = passwordErrorMessage.Replace("password: ", string.Empty);
            }

            return result;
        }
        
        private UserProfileUpdateResult UpdateUsername(UserProfile userProfile, ProfileFields profileFields, User user)
        {
            var result = new UserProfileUpdateResult();

            var client = new RestClient(_settings.IAMDomain);
            var request = new RestRequest($"/api/v1/users/{userProfile.Id}", Method.POST);
            Console.WriteLine("RestRequest: /api/v1/users/{userProfile.Id}");
            request.AddHeader("Authorization", $"SSWS {_settings.ApiKey}");
            
            request.AddParameter("application/json", JsonConvert.SerializeObject(userProfile), ParameterType.RequestBody);
            var response = client.Execute(request);

            Console.WriteLine(response.Content);

            if (response.IsSuccessful)
            {
                RemoveUserCacheObjects(user.Username);
            }
            else
            {
                var errorMessage = string.Empty;
                try
                {
                    var error = JsonConvert.DeserializeObject<Error>(response.Content);
                    errorMessage = error.ErrorCauses.First().ErrorSummary;
                }
                catch
                {                                    
                }

                if (errorMessage.Contains("An object with this field already exists in the current organization"))
                {
                    _authenticationLogService.LogMessage(user, EventNameType.UsernameMustBeUnique);
                    result.Reason = UserProfileUpdateResultReason.UsernameMustBeUnique;
                }
                else
                {
                    result.ErrorMessage = errorMessage;
                    result.Reason = UserProfileUpdateResultReason.Error;
                }
            }

            return result;
        }

        public bool Authenticate(string username, string password)
        {
            var result = false;
            var client = new RestClient(_settings.IAMDomain);
            var request = new RestRequest("/api/v1/authn", Method.POST);
            Console.WriteLine("RestRequest: /api/v1/authn");
            request.AddHeader("Authorization", $"SSWS {_settings.ApiKey}");

            var requestBody = new 
            {
                username = username,
                password = password
            };

            request.AddJsonBody(requestBody); 

            var response = client.Execute(request);

            Console.WriteLine(response.Content);

            if (response.IsSuccessful)
            {
                result = true;
            }

            return result;
        }        

        public string CreateSession()
        {
            var result = string.Empty;
            var user = GetCachedUser();
            if (user != null
                && !string.IsNullOrEmpty(user.MigratedUserId))
            {
                var currentDate = DateTime.Now;
                var sessionGuid = System.Guid.NewGuid();
                _context.UserSessions.Add(new UserSession {
                    UserId = null,
                    MigratedUserId = user.MigratedUserId,
                    SessionGuid = sessionGuid.ToString(),
                    ExpirationDate = currentDate.AddMinutes(_settings.SessionTime),
                    CreatedById = user.MigratedUserId,
                    CreatedDate = DateTime.Now
                });                

                _context.SaveChanges();

                result = sessionGuid.ToString();
            }

            return result;
        }

        public bool IsValidSession(string sessionGuid)
        {
            var userSession = GetSession(sessionGuid);

            return userSession != null;
        }

        public bool EndSession(string sessionGuid)
        {
            var success = false;
            var userSession = GetSession(sessionGuid);
            if (userSession != null)
            {
                var currentDate = DateTime.Now;
                userSession.ExpirationDate = currentDate;

                var outdatedSessionDate = DateTime.Now.AddMinutes(-90);
                var outdatedUserSessions = _context.UserSessions.Where(x => x.ExpirationDate <= outdatedSessionDate);

                if (outdatedUserSessions.Any())
                {
                    _context.UserSessions.RemoveRange(outdatedUserSessions);
                }

                var outdatedAuthenticationLogDate = DateTime.Now.AddDays(-30);
                var outdatedAuthenticationLogs = _context.AuthenticationLogs.Where(x => x.CreatedDate <= outdatedAuthenticationLogDate);

                if (outdatedAuthenticationLogs.Any())
                {
                    _context.AuthenticationLogs.RemoveRange(outdatedAuthenticationLogs);
                }

                _authenticationLogService.LogMessage(new User { MigratedUserId = userSession.MigratedUserId }, EventNameType.SignOut);

                _context.SaveChanges();

                success = true;
            }            

            return success;
        }

        public bool RenewSession(string sessionGuid)
        {
            var success = false;
            var userSession = GetSession(sessionGuid);
            if (userSession != null)
            {
                var currentDate = DateTime.Now;
                userSession.ExpirationDate = currentDate.AddMinutes(_settings.SessionTime);
                userSession.UpdatedById = userSession.MigratedUserId;
                userSession.UpdatedDate = DateTime.Now;

                _context.SaveChanges();

                success = true;
            }            

            return success;
        }        

        public UserSession GetSession(string sessionGuid)
        {
            var currentDate = DateTime.Now;
            var userSession = _context.UserSessions
                .FirstOrDefault(x => x.SessionGuid == sessionGuid && x.ExpirationDate > currentDate);
            
            return userSession;
        }

        public User GetCurrentUser(string sessionGuid)
        {
            User user = null;
            if (!string.IsNullOrEmpty(sessionGuid))
            {
                var userSession = GetSession(sessionGuid);
                if (userSession != null)
                {
                    user = GetCachedUser();
                }
            }
            
            return user;
        }

        private void RemoveUserCacheObjects(string username)
        {
            var userId = GetCurrentUserId();
            if (userId != null && username != null)
            {
                _memoryCache.Remove(userId + "_UserProfileById");
                _memoryCache.Remove(userId + "_UserGroupsById");
                _memoryCache.Remove(userId + "_User");
            }
        }
    }
}
