using System;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Okta.AspNetCore;

namespace AuthenticationExample.Helpers
{
    public static class IAMConfiguration
    {
        public const string IAMProviderName = "OKTA";

        public static void Register(IServiceCollection services, IConfiguration configuration)
        {
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            })
            .AddCookie(options =>
            {
                options.LoginPath = new PathString("/Home/Index");
            })
            .AddOktaMvc(new OktaMvcOptions
            {
                OktaDomain = configuration.GetValue<string>("Settings:IAMDomain"),
                ClientId = configuration.GetValue<string>("Settings:ClientId"),
                ClientSecret = configuration.GetValue<string>("Settings:ClientSecret"),
                Scope = new List<string> { "openid", "profile", "email" },
            });
        }

        public static string GetChallengeScheme()
        {
            return OktaDefaults.MvcAuthenticationScheme;
        }

        public static IActionResult ReturnSignOutResult()
        {
            return new SignOutResult(
                new[]
                {
                    OktaDefaults.MvcAuthenticationScheme,
                    CookieAuthenticationDefaults.AuthenticationScheme,
                },
                new AuthenticationProperties { RedirectUri = "/Home/SignedOut" });
        }
    }
}