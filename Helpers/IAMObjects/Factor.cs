using Newtonsoft.Json;
using System.Collections.Generic;

namespace AuthenticationExample.Helpers.IAMObjects
{
    public class Factor
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("factorType")]
        public string FactorType { get; set; }
    }
}