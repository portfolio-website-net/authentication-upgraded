using Newtonsoft.Json;
using System.Collections.Generic;

namespace AuthenticationExample.Helpers.IAMObjects
{
    public class Error
    {
        [JsonProperty("errorCode")]
        public string ErrorCode { get; set; }

        [JsonProperty("errorSummary")]
        public string ErrorSummary { get; set; }

        [JsonProperty("errorLink")]
        public string ErrorLink { get; set; }

        [JsonProperty("errorId")]
        public string ErrorId { get; set; }

        [JsonProperty("errorCauses")]
        public ErrorCause[] ErrorCauses { get; set; }
    }

    public class ErrorCause
    {
        [JsonProperty("errorSummary")]
        public string ErrorSummary { get; set; }
    }
}