using Newtonsoft.Json;
using System.Collections.Generic;

namespace AuthenticationExample.Helpers.IAMObjects
{
    public class UserProfile
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "profile")]
        public Profile Profile { get; set; }
    }

    public class CreateUser
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "profile")]
        public Profile Profile { get; set; }

        [JsonProperty(PropertyName = "credentials")]
        public Credentials Credentials { get; set; }

        [JsonProperty(PropertyName = "groupIds")]
        public List<string> GroupIds { get; set; }
    }

    public class Profile
    {
        [JsonProperty(PropertyName = "firstName")]
        public string FirstName { get; set; }

        [JsonProperty(PropertyName = "lastName")]
        public string LastName { get; set; }

        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }

        [JsonProperty(PropertyName = "login")]
        public string Username { get; set; }

        [JsonProperty(PropertyName = "primaryPhone")]
        public string PrimaryPhone { get; set; }
    }

    public class Credentials
    {
        [JsonProperty(PropertyName = "password")]
        public Password Password { get; set; }
    }

    public class Password
    {
        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; }
    }

    public class Group
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "profile")]
        public GroupProfile Profile { get; set; }
    }

    public class GroupProfile
    {
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }
    }
}