namespace AuthenticationExample.Helpers
{
    public enum EventNameType
    {
        FailedSignIn,
        SignIn,
        SignOut,
        PageView,
        ProfileUpdate,
        UsernameUpdate,
        PasswordUpdate,
        PermissionUpdate,
        PermissionUpdateError,
        InvalidProfilePassword,
        UsernameMustBeUnique,
        PasswordsDoNotMatch,
        PasswordUpdateError,
        ProfileUpdateError,
        NoAccessToSecurePage,
        AccessDenied,
        SuccessfulMigration,
        FailedMigration,
        SMSFactorEnrollment,
        SMSFactorEnrollmentError
    }
}