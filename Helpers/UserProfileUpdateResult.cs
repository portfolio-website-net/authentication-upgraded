namespace AuthenticationExample.Helpers
{
    public class UserProfileUpdateResult
    {
        public UserProfileUpdateResultReason Reason { get; set; }

        public string ErrorMessage { get; set; }
    }

    public enum UserProfileUpdateResultReason
    {
        None,
        Error,
        InvalidCurrentPassword,
        PasswordsDoNotMatch,
        PasswordUpdateError,
        UsernameMustBeUnique,
        SMSUpdateError,
        PermissionUpdateError,        
        Success
    }
}