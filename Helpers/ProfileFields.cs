using System.Collections.Generic;

namespace AuthenticationExample.Helpers
{
    public class ProfileFields
    {
        public int UserId { get; set; }

        public string CurrentPassword { get; set; }
        
        public string Username { get; set; }

        public string Password { get; set; }

        public string PasswordConfirm { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public List<string> Claims { get; set; }
    }
}