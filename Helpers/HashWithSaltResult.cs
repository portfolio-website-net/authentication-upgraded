namespace AuthenticationExample.Helpers
{
    // Source: https://dotnetcodr.com/2017/10/26/how-to-hash-passwords-with-a-salt-in-net-2/
    public class HashWithSaltResult
    {
        public string Salt { get; }
        public string Digest { get; set; }
    
        public HashWithSaltResult(string salt, string digest)
        {
            Salt = salt;
            Digest = digest;
        }
    }
}