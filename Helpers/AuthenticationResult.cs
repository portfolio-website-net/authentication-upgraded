namespace AuthenticationExample.Helpers
{
    public class AuthenticationResult
    {
        public string Username { get; set; }

        public AuthenticationResultType AuthenticationResultType { get; set; }

        public string AuthenticationResultTypeValue
        {
            get
            {
                return this.AuthenticationResultType.ToString();
            }
        }
    }

    public enum AuthenticationResultType
    {
        None,
        InvalidCredentials,
        SignedOut,
        UseLegacySignIn
    }
}