using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace AuthenticationExample.Models
{
    public class UserSession
    { 
        [Key]
        public int UserSessionId { get; set; }

        public int? UserId { get; set; }

        public string MigratedUserId { get; set; }

        public string SessionGuid { get; set; }

        public DateTime ExpirationDate { get; set; }

        public string CreatedById { get; set; }

        public DateTime CreatedDate { get; set; }

        public string UpdatedById { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}