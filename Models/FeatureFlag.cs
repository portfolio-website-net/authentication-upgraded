using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace AuthenticationExample.Models
{
    public class FeatureFlag
    { 
        [Key]
        public int FeatureFlagId { get; set; }

        public int FeatureFlagTypeId { get; set; }

        public int? UserId { get; set; }

        public string MigratedUserId { get; set; }

        public string CreatedById { get; set; }

        public DateTime CreatedDate { get; set; }

        public string UpdatedById { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}