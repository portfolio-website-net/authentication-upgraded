using Microsoft.EntityFrameworkCore;
 
namespace AuthenticationExample.Models
{
    public class Context : DbContext
    {
        public Context (DbContextOptions<Context> options)
            : base(options)
        {
        }
 
        public DbSet<AuthenticationLog> AuthenticationLogs { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<UserSession> UserSessions { get; set; }

        public DbSet<FeatureFlag> FeatureFlags { get; set; }

        public DbSet<FeatureFlagType> FeatureFlagTypes { get; set; }
    }
}