using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace AuthenticationExample.Models
{
    public class AuthenticationLog
    { 
        [Key]
        public int AuthenticationLogId { get; set; }

        public int? UserId { get; set; }

        public string MigratedUserId { get; set; }

        public string EventName { get; set; }

        public string Message { get; set; }

        public string Path { get; set; }

        public string IpAddress { get; set; }

        public string UserAgent { get; set; }

        public string CreatedById { get; set; }

        public DateTime CreatedDate { get; set; }

        public string UpdatedById { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}