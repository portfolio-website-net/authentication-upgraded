using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Collections.Generic;
 
namespace AuthenticationExample.Models
{
    public class User
    { 
        [Key]
        public int UserId { get; set; }

        public string MigratedUserId { get; set; }

        public string Username { get; set; }

        public string PasswordHash { get; set; }

        public string PasswordSalt { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }        

        public string Email { get; set; }

        public string Phone { get; set; }

        public string Claims { get; set; }

        public int CreatedById { get; set; }

        public DateTime CreatedDate { get; set; }

        public int? UpdatedById { get; set; }

        public DateTime? UpdatedDate { get; set; }        

        [NotMapped]
        public List<string> ClaimsList
        {
            get
            {
                if (!string.IsNullOrEmpty(this.Claims))
                {
                    return this.Claims.Split(',').Select(x => x.Trim()).ToList();
                }
                else
                {
                    return new List<string>();
                }
            }
        }

        [NotMapped]
        public bool IsAdmin
        {
            get
            {
                if (this.ClaimsList.Any(x => x.Trim() == "Admin"))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}