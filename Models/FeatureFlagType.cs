using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace AuthenticationExample.Models
{
    public class FeatureFlagType
    { 
        [Key]
        public int FeatureFlagTypeId { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public string CreatedById { get; set; }

        public DateTime CreatedDate { get; set; }

        public string UpdatedById { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}