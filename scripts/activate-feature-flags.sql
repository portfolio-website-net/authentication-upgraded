INSERT INTO [dbo].[FeatureFlags] ([FeatureFlagTypeId], [UserId], [CreatedById], [CreatedDate])
SELECT (SELECT [FeatureFlagTypeId] FROM [dbo].[FeatureFlagTypes] WHERE Code = 'EnableUserIAMMigration'),
       [UserId],
	   0,
	   GetDate()
  FROM [dbo].[Users]