IF OBJECT_ID('dbo.[FeatureFlagTypes]', 'U') IS NOT NULL 
BEGIN 
	IF OBJECT_ID('dbo.[FeatureFlags]', 'U') IS NOT NULL 
	BEGIN 
		DROP TABLE dbo.[FeatureFlags] 
	END
	
	DROP TABLE dbo.[FeatureFlagTypes] 
END

CREATE TABLE [dbo].[FeatureFlagTypes](
	[FeatureFlagTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Code] [nvarchar](50) NOT NULL,	
	[CreatedById] [nvarchar](20) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedById] [nvarchar](20) NULL,
	[UpdatedDate] [datetime] NULL,
	CONSTRAINT [PK_FeatureFlagTypes] PRIMARY KEY CLUSTERED 
	(
		[FeatureFlagTypeId] ASC
	)
) 