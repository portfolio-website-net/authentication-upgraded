/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Users
	(
	UserId int NOT NULL IDENTITY (1, 1),
	MigratedUserId nvarchar(20) NULL,
	Username nvarchar(100) NOT NULL,
	PasswordHash nvarchar(100) NOT NULL,
	PasswordSalt nvarchar(100) NOT NULL,
	FirstName nvarchar(100) NOT NULL,
	LastName nvarchar(100) NOT NULL,
	Email nvarchar(100) NOT NULL,
	Phone nvarchar(100) NOT NULL,
	Claims nvarchar(100) NOT NULL,
	CreatedById int NOT NULL,
	CreatedDate datetime NOT NULL,
	UpdatedById int NULL,
	UpdatedDate datetime NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Users SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_Users ON
GO
IF EXISTS(SELECT * FROM dbo.Users)
	 EXEC('INSERT INTO dbo.Tmp_Users (UserId, Username, PasswordHash, PasswordSalt, FirstName, LastName, Email, Phone, Claims, CreatedById, CreatedDate, UpdatedById, UpdatedDate)
		SELECT UserId, Username, PasswordHash, PasswordSalt, FirstName, LastName, Email, Phone, Claims, CreatedById, CreatedDate, UpdatedById, UpdatedDate FROM dbo.Users WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Users OFF
GO
ALTER TABLE dbo.UserSessions
	DROP CONSTRAINT FK_UserSessions_Users
GO
DROP TABLE dbo.Users
GO
EXECUTE sp_rename N'dbo.Tmp_Users', N'Users', 'OBJECT' 
GO
ALTER TABLE dbo.Users ADD CONSTRAINT
	PK_Users PRIMARY KEY CLUSTERED 
	(
	UserId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
select Has_Perms_By_Name(N'dbo.Users', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Users', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Users', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.UserSessions ADD CONSTRAINT
	FK_UserSessions_Users FOREIGN KEY
	(
	UserId
	) REFERENCES dbo.Users
	(
	UserId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.UserSessions SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.UserSessions', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.UserSessions', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.UserSessions', 'Object', 'CONTROL') as Contr_Per 







/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_UserSessions
	(
	UserSessionId int NOT NULL IDENTITY (1, 1),
	UserId int NULL,
	MigratedUserId nvarchar(20) NULL,
	SessionGuid nvarchar(36) NOT NULL,
	ExpirationDate datetime NOT NULL,
	CreatedById nvarchar(20) NOT NULL,
	CreatedDate datetime NOT NULL,
	UpdatedById nvarchar(20) NULL,
	UpdatedDate datetime NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_UserSessions SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_UserSessions ON
GO
IF EXISTS(SELECT * FROM dbo.UserSessions)
	 EXEC('INSERT INTO dbo.Tmp_UserSessions (UserSessionId, UserId, SessionGuid, ExpirationDate, CreatedById, CreatedDate, UpdatedById, UpdatedDate)
		SELECT UserSessionId, UserId, SessionGuid, ExpirationDate, CONVERT(nvarchar(20), CreatedById), CreatedDate, CONVERT(nvarchar(20), UpdatedById), UpdatedDate FROM dbo.UserSessions WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_UserSessions OFF
GO
DROP TABLE dbo.UserSessions
GO
EXECUTE sp_rename N'dbo.Tmp_UserSessions', N'UserSessions', 'OBJECT' 
GO
ALTER TABLE dbo.UserSessions ADD CONSTRAINT
	PK_UserSessions PRIMARY KEY CLUSTERED 
	(
	UserSessionId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
select Has_Perms_By_Name(N'dbo.UserSessions', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.UserSessions', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.UserSessions', 'Object', 'CONTROL') as Contr_Per 
