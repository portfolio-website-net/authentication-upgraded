IF OBJECT_ID('dbo.[FeatureFlags]', 'U') IS NOT NULL 
BEGIN 
	DROP TABLE dbo.[FeatureFlags] 
END

CREATE TABLE [dbo].[FeatureFlags](
	[FeatureFlagId] [int] IDENTITY(1,1) NOT NULL,
	[FeatureFlagTypeId] [int] NOT NULL,
	[UserId] [int] NULL,
	[MigratedUserId] [nvarchar](20) NULL,	
	[CreatedById] [nvarchar](20) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedById] [nvarchar](20) NULL,
	[UpdatedDate] [datetime] NULL,
	CONSTRAINT [PK_FeatureFlags] PRIMARY KEY CLUSTERED 
	(
		[FeatureFlagId] ASC
	)
) 

ALTER TABLE [dbo].[FeatureFlags]  WITH CHECK ADD  CONSTRAINT [FK_FeatureFlags_FeatureFlagTypes] FOREIGN KEY([FeatureFlagTypeId])
REFERENCES [dbo].[FeatureFlagTypes] ([FeatureFlagTypeId])
GO

ALTER TABLE [dbo].[FeatureFlags] CHECK CONSTRAINT [FK_FeatureFlags_FeatureFlagTypes]
GO


INSERT INTO [dbo].[FeatureFlagTypes] ([Name], [Code], [CreatedById], [CreatedDate])
SELECT 'Enable User IAM Migration', 'EnableUserIAMMigration', 0, GetDate()
