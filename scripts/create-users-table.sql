IF OBJECT_ID('dbo.[Users]', 'U') IS NOT NULL 
BEGIN 
	IF OBJECT_ID('dbo.[UserSessions]', 'U') IS NOT NULL 
	BEGIN 
		DROP TABLE dbo.[UserSessions] 
	END
	
	DROP TABLE dbo.[Users] 
END

CREATE TABLE [dbo].[Users](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](100) NOT NULL,
	[PasswordHash] [nvarchar](100) NOT NULL,
	[PasswordSalt] [nvarchar](100) NOT NULL,
	[FirstName] [nvarchar](100) NOT NULL,
	[LastName] [nvarchar](100) NOT NULL,
	[Email] [nvarchar](100) NOT NULL,
	[Phone] [nvarchar](100) NOT NULL,
	[Claims] [nvarchar](100) NOT NULL,
	[CreatedById] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedById] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
	(
		[UserId] ASC
	)
) 
