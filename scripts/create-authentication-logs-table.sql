IF OBJECT_ID('dbo.[AuthenticationLogs]', 'U') IS NOT NULL 
BEGIN 
	DROP TABLE dbo.[AuthenticationLogs] 
END

CREATE TABLE [dbo].[AuthenticationLogs](
	[AuthenticationLogId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[EventName] [nvarchar](100) NOT NULL,
	[Message] [nvarchar](max) NULL,
	[Path] [nvarchar](max) NULL,
	[IpAddress] [nvarchar](50) NULL,	
	[UserAgent] [nvarchar](max) NULL,
	[CreatedById] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedById] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	CONSTRAINT [PK_AuthenticationLogs] PRIMARY KEY CLUSTERED 
	(
		[AuthenticationLogId] ASC
	)
) 
GO





/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_AuthenticationLogs
	(
	AuthenticationLogId int NOT NULL IDENTITY (1, 1),
	UserId int NULL,
	MigratedUserId nvarchar(20) NULL,
	EventName nvarchar(100) NOT NULL,
	Message nvarchar(MAX) NULL,
	Path nvarchar(MAX) NULL,
	IpAddress nvarchar(50) NULL,
	UserAgent nvarchar(MAX) NULL,
	CreatedById nvarchar(20) NOT NULL,
	CreatedDate datetime NOT NULL,
	UpdatedById nvarchar(20) NULL,
	UpdatedDate datetime NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_AuthenticationLogs SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_AuthenticationLogs ON
GO
IF EXISTS(SELECT * FROM dbo.AuthenticationLogs)
	 EXEC('INSERT INTO dbo.Tmp_AuthenticationLogs (AuthenticationLogId, UserId, EventName, Message, Path, IpAddress, UserAgent, CreatedById, CreatedDate, UpdatedById, UpdatedDate)
		SELECT AuthenticationLogId, UserId, EventName, Message, Path, IpAddress, UserAgent, CONVERT(nvarchar(20), CreatedById), CreatedDate, CONVERT(nvarchar(20), UpdatedById), UpdatedDate FROM dbo.AuthenticationLogs WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_AuthenticationLogs OFF
GO
DROP TABLE dbo.AuthenticationLogs
GO
EXECUTE sp_rename N'dbo.Tmp_AuthenticationLogs', N'AuthenticationLogs', 'OBJECT' 
GO
ALTER TABLE dbo.AuthenticationLogs ADD CONSTRAINT
	PK_AuthenticationLogs PRIMARY KEY CLUSTERED 
	(
	AuthenticationLogId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
select Has_Perms_By_Name(N'dbo.AuthenticationLogs', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.AuthenticationLogs', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.AuthenticationLogs', 'Object', 'CONTROL') as Contr_Per 
