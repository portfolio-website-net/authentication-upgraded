IF OBJECT_ID('dbo.[UserSessions]', 'U') IS NOT NULL 
BEGIN 
	DROP TABLE dbo.[UserSessions] 
END

CREATE TABLE [dbo].[UserSessions](
	[UserSessionId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[SessionGuid] [nvarchar](36) NOT NULL,
	[ExpirationDate] [datetime] NOT NULL,
	[CreatedById] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedById] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	CONSTRAINT [PK_UserSessions] PRIMARY KEY CLUSTERED 
	(
		[UserSessionId] ASC
	)
) 
GO

ALTER TABLE [dbo].[UserSessions]  WITH CHECK ADD  CONSTRAINT [FK_UserSessions_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserId])
GO

ALTER TABLE [dbo].[UserSessions] CHECK CONSTRAINT [FK_UserSessions_Users]
GO