SELECT ISNULL(u.[Username], 'Anonymous') AS [User],
       al.[EventName],
       al.[Path],
       al.[IpAddress],
       al.[UserAgent],
       al.[CreatedDate] AS [Date]
  FROM [AuthenticationLogs] al
  LEFT JOIN [Users] u ON al.UserId = u.UserId
 ORDER BY al.CreatedDate