﻿namespace AuthenticationExample
{
    public class Settings
    {
        public string IAMDomain { get; set; }

        public string ClientId { get; set; }

        public string ClientSecret { get; set; }

        public string ApiKey { get; set; }        

        public string SessionCookieName { get; set; }

        public int SessionTime { get; set; }
    }
}
