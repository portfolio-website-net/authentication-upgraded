﻿using System;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Diagnostics;
using AuthenticationExample.Models;
using AuthenticationExample.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using AuthenticationExample.Helpers;
using AuthenticationExample.Controllers.Base;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace AuthenticationExample.Controllers
{
    [ClaimRequirement(SecureClaimTypes.Permission, "Admin,Read")]
    public class SecureController : BaseController
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly IUserProfileService _userProfileService;
        private readonly IIAMService _iamService;

        public SecureController(IAuthenticationService authenticationService,
            IUserProfileService userProfileService,
            IIAMService iamService)
        {
            _authenticationService = authenticationService;
            _userProfileService = userProfileService;
            _iamService = iamService;
        }
        
        public IActionResult Index()
        {
            return View();
        }

        [ClaimRequirement(SecureClaimTypes.Permission, "Admin")]
        public IActionResult Admin()
        {
            return View();
        }

        public IActionResult NoAccess()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Profile()
        {
            return View(new UserProfileUpdateResult());
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public IActionResult Profile([FromForm] ProfileFields profileFields)
        {
            var result = new UserProfileUpdateResult
            {
                Reason = UserProfileUpdateResultReason.Error
            };
            var user = (User)HttpContext.Items["User"];
            if (user != null)
            {
                if (user.MigratedUserId != null)
                {
                    result = _iamService.UpdateUserProfile(profileFields);
                    user = _iamService.GetCachedUser();
                    HttpContext.Items["User"] = user;
                }
                else
                {
                    profileFields.UserId = user.UserId;
                    result = _userProfileService.UpdateUserProfile(profileFields);
                }                
            }

            return View(result);
        }
    }
}
