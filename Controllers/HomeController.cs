﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using AuthenticationExample.Models;
using Interfaces = AuthenticationExample.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using AuthenticationExample.Controllers.Base;
using AuthenticationExample.Helpers;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication;
using System.Linq;

namespace AuthenticationExample.Controllers
{
    [ClaimRequirement(SecureClaimTypes.Permission, "")]
    public class HomeController : BaseController
    {        
        private readonly Interfaces.IAuthenticationService _authenticationService;        
        private readonly Settings _settings;
        private readonly Interfaces.IIAMService _iamService;

        public HomeController(Interfaces.IAuthenticationService authenticationService, 
            IOptions<Settings> settings,
            Interfaces.IIAMService iamService)
        {
            _authenticationService = authenticationService;            
            _settings = settings.Value;
            _iamService = iamService;
        }
        
        [HttpGet]
        public IActionResult Index()
        {
            _authenticationService.PopulateUsersIfEmptyDatabase();

            if (_iamService.HasUserIAMSession())        
            {
                var sessionId = _iamService.CreateSession();
                if (!string.IsNullOrEmpty(sessionId))
                {
                    var options = new CookieOptions
                    {
                        IsEssential = true,
                        HttpOnly = true,
                        Secure = true
                    };

                    Response.Cookies.Append(_settings.SessionCookieName, sessionId, options);

                    return RedirectToAction("Index", "Secure");
                }
            }

            return View();
        }

        [HttpPost]
        public IActionResult GetSignInResult(string username, string password) 
        {
            var authenticationResult = new AuthenticationResult();

            if (_iamService.IsUserReadyForMigration(username))
            {
                if (_authenticationService.Authenticate(username, password))
                {
                    if (_iamService.MigrateUser(username, password))
                    {
                        authenticationResult.AuthenticationResultType = AuthenticationResultType.None;
                    }
                    else
                    {
                        authenticationResult.AuthenticationResultType = AuthenticationResultType.UseLegacySignIn;
                    }
                }
                else
                {
                    authenticationResult.AuthenticationResultType = AuthenticationResultType.None;
                }
            }
            else if (_iamService.IsUserMigrated(username))
            {
                authenticationResult.AuthenticationResultType = AuthenticationResultType.None;
            }
            else
            {
                if (_authenticationService.Authenticate(username, password))
                {
                    authenticationResult.AuthenticationResultType = AuthenticationResultType.UseLegacySignIn;
                }
                else
                {
                    authenticationResult.AuthenticationResultType = AuthenticationResultType.None;
                }
            }

            return Json(authenticationResult);
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public IActionResult LegacySignIn(string username, string password) 
        {
            var authenticationResult = new AuthenticationResult();
            var success = _authenticationService.Authenticate(username, password);
            if (success)
            {
                var sessionId = _authenticationService.CreateSession(username);
                var options = new CookieOptions
                {
                    IsEssential = true,
                    HttpOnly = true,
                    Secure = true
                };

                Response.Cookies.Append(_settings.SessionCookieName, sessionId, options);

                return RedirectToAction("Index", "Secure");
            }
            else
            {
                authenticationResult.Username = username;
                authenticationResult.AuthenticationResultType = AuthenticationResultType.InvalidCredentials;
                return View("Index", authenticationResult);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SignIn([FromForm]string sessionToken)
        {
            if (!HttpContext.User.Identity.IsAuthenticated)
            {                
                var properties = new AuthenticationProperties();
                properties.Items.Add("sessionToken", sessionToken);
                properties.RedirectUri = "/Home/";

                return Challenge(properties, IAMConfiguration.GetChallengeScheme());
            }                        

            return RedirectToAction("Index", "Home");
        }

        public IActionResult LegacySignOut()
        {
            var sessionCookie = Request.Cookies[_settings.SessionCookieName];
            if (sessionCookie != null)
            {
                _authenticationService.EndSession(sessionCookie);

                var options = new CookieOptions
                {
                    IsEssential = true,
                    HttpOnly = true,
                    Secure = true,
                    Expires = DateTime.Now.AddDays(-1)
                };

                Response.Cookies.Append(_settings.SessionCookieName, string.Empty, options);
                HttpContext.Items["User"] = null;
            }

            var authenticationResult = new AuthenticationResult
            {
                AuthenticationResultType = AuthenticationResultType.SignedOut
            };
            return View("Index", authenticationResult);
        }

        [HttpPost]
        public IActionResult SignOut()
        {
            if (_iamService.HasUserIAMSession())        
            {
                var sessionCookie = Request.Cookies[_settings.SessionCookieName];
                if (sessionCookie != null)
                {
                    _iamService.EndSession(sessionCookie);

                    var options = new CookieOptions
                    {
                        IsEssential = true,
                        HttpOnly = true,
                        Secure = true,
                        Expires = DateTime.Now.AddDays(-1)
                    };

                    Response.Cookies.Append(_settings.SessionCookieName, string.Empty, options);
                    HttpContext.Items["User"] = null;
                }

                return IAMConfiguration.ReturnSignOutResult();
            }
            else
            {
                return LegacySignOut();
            }            
        }

        public IActionResult SignedOut()
        {
            var authenticationResult = new AuthenticationResult
            {
                AuthenticationResultType = AuthenticationResultType.SignedOut
            };
            return View("Index", authenticationResult);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
